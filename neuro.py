import numpy as np
import keras
from keras.utils.data_utils import get_file
from keras.datasets import reuters
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation
from keras.preprocessing.text import Tokenizer
from sklearn.datasets import fetch_20newsgroups




newsgroups_train = fetch_20newsgroups(subset='train')
newsgroups_test = fetch_20newsgroups(subset='test')



max_words = 1000
epochs = 5
batch_size = 32


tokenizer = Tokenizer(num_words=max_words)
tokenizer.fit_on_texts(newsgroups_train["data"])  # теперь токенизатор знает словарь для этого корпуса текстов

x_train = tokenizer.texts_to_matrix(newsgroups_train["data"], mode='binary')
x_test = tokenizer.texts_to_matrix(newsgroups_test["data"], mode='binary')



print(x_train)
print(x_test)

num_classes = np.max(newsgroups_train["target"]) + 1

y_train = keras.utils.to_categorical(newsgroups_train["target"], num_classes)
y_test = keras.utils.to_categorical(newsgroups_test["target"], num_classes)



model = Sequential()
model.add(Dense(512, input_shape=(max_words,)))
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(num_classes))
model.add(Activation('softmax'))


model.compile(loss='categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])


history = model.fit(x_train, y_train,
                    batch_size=batch_size,
                    epochs=epochs,
                    verbose=1,
                    validation_data=(x_test, y_test),
                    validation_split=0.5)

score = model.evaluate(x_test, y_test, verbose=1)
print("Test Loss: ", score[0])
print("Test Accuracy: ", score[1])


# print(model.predict_on_batch(x_test[0]))

model_json = model.to_json()
with open("model.json", "w") as json_file:
	json_file.write(model_json)

model.save_weights("model.h5")





# max_words = 1000
# batch_size = 32
# epochs = 2

# print('Loading data...')
# (x_train, y_train), (x_test, y_test) = reuters.load_data(num_words=max_words, test_split=0.2)


# target_x = [x_test[0]]
# _y_test = y_test


# num_classes = np.max(y_train) + 1


# tokenizer = Tokenizer(num_words=max_words)
# x_train = tokenizer.sequences_to_matrix(x_train, mode='binary')
# x_test = tokenizer.sequences_to_matrix(x_test, mode='binary')

# y_train = keras.utils.to_categorical(y_train, num_classes)
# y_test = keras.utils.to_categorical(y_test, num_classes)


# target_x = tokenizer.sequences_to_matrix(target_x, mode="binary")
# # target_y = keras.utils.to_categorical(target_y, num_classes)



# model = Sequential()
# model.add(Dense(512, input_shape=(max_words,)))
# model.add(Activation('relu'))
# model.add(Dropout(0.5))
# model.add(Dense(num_classes))
# model.add(Activation('softmax'))

# model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

# history = model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs, verbose=1, validation_split=0.1)
# score = model.evaluate(x_test, y_test, batch_size=batch_size, verbose=1)


# for index, i in enumerate(y_test[:10]):
# 	target_y = [_y_test[index]]
# 	target_data = model.evaluate(target_x, keras.utils.to_categorical(target_y, num_classes))
# 	# print("Target data: ", target_data)

# 	print("LOSS: ", target_data[0])
# 	print("Accuracy: ", target_data[1])
