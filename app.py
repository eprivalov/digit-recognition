from flask import Flask, render_template, request
from scipy.misc import imsave, imread, imresize
import numpy as np
import keras.models
import re
import sys
import os
from PIL import Image
from base64 import b64decode

sys.path.append(os.path.abspath("./model"))

from load import *
app = Flask(__name__)
global model, graph
model, graph = init()


def convertImage(imgData):
	import base64
	d = re.search(r"base64,(.*)", str(imgData)[:-1]).group(1)
	data = d.replace(' ', '+')

	with open(os.getcwd()+"/img.png", "wb") as output:
		output.write(base64.b64decode(data))


@app.route("/")
def index():
	return render_template("index.html")


@app.route("/predict/", methods=["GET", "POST"])
def predict():
	imgData = request.get_data()
	convertImage(imgData)
	x = imread(os.getcwd()+"/img.png", mode="L")
	x = np.invert(x)
	x = imresize(x, (28, 28))
	print(x.shape)
	x = x.reshape((1, 28, 28, 1))
	with graph.as_default():
		out = model.predict(x)
		response = np.array_str(np.argmax(out, axis=1))
		return response


if __name__ == '__main__':
	port = int(os.environ.get("PORT", 5000))
	app.run(host="0.0.0.0", port=port)
